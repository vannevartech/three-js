THREE.js Web Components
========================

This is a bundle of web components and associated JavaScript files that wrap THREE.js for use with Polymer.
[Three JS](http://threejs.org/)
[Polymer](https://www.polymer-project.org/)
Using this repo you can easily create an interactive 3D viewport for use on any webpage.

If you are setting up this repository for the first time you can follow the instructions for getting set up with Polymer.
https://www.polymer-project.org/1.0/docs/start/getting-the-code.html
https://www.polymer-project.org/1.0/docs/start/reusableelements.html

Once you have done that you can view the demo page from localhost.
Run polyserve and navigate your browser to:
[Demo](http://localhost:8080/components/three-js/three-viewport.demo.html)

To run tests locally with web-component-tester use the following command:
wct -l chrome ./*test*
Currently only Chrome is supported.

![three-viewport](https://bytebucket.org/vannevartech/three-js/raw/9e77d7992b5e47dde19c8f3a16013ba7340fcc24/demo.png "three-viewport")

We are now using gerrit to manage contributions.
To set up gerrit follow these instrucitons. Replace $USER with your gerrit username.
git remote add gerrit ssh://$USER@cr.vannevartech.com:29418/three-js
curl https://cr.vannevartech.com/tools/hooks/commit-msg > .git/hooks/commit-msg
chmod +x .git/hooks/commit-msg

Then when you are ready to push, use this command.
git push gerrit HEAD:refs/for/master
